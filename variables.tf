variable "nro" {

}
variable "vm_name" {

}
variable "machine_size" {

}
variable "region" {

}
variable "zone" {

}
variable "labels" {

}

variable "project" {

}

variable "airbyte_version" {

}
variable "sql_version" {

}

variable "user" {
}

variable "disk_size_gb" {

}
# DNS Variable

# variable "dns_zone_name" {
#   type = string
# }

variable "dns_primary_zone" {
  type = string
}

variable "dns_primary_zone_name" {
  type = string
}

variable "web_server_rules_source_range_1" {
}

variable "web_server_rules_source_range_2" {
}

# IAP variables
# variable "iap_support_email" {
#   type        = string
#   description = "Support email to be shown on IAP page when login is unsuccessful"
# }

variable "iap_application_title" {
  type        = string
  description = "Name of the application, will be displayed together with email on failed login"
  default     = "airbyte-cosmos"
}

variable "iap_display_name" {
  type        = string
  description = "Human friendly name for oauth client"
  default     = "airbyte-oauth"
}

variable "iap_iam_https_resource_accessor_members" {
  type        = list(string) #formerly list(string), but needs to be decoded from gitlabci using jsondecode first
  description = "Members to add as (HTTPS) accessors to the app. Must be in the orgagnization already. Format is <member type>:<email / domain>. More information: https://cloud.google.com/billing/docs/reference/rest/v1/Policy#Binding"
}

variable "image_name" {
  type    = string
  default = "cosmos-airbyte-packer"
}

variable "env_file_path" {
}